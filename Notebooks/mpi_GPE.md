---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.7
kernelspec:
  display_name: Python 3 (parallel-gpe)
  language: python
  name: parallel-gpe
---

```{code-cell} ipython3
import logging; logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
import mmf_setup;mmf_setup.nbinit()
from parallel_gpe.mpi_GPE import State
%load_ext autoreload
```

```{code-cell} ipython3
%autoreload
s = State(M=5, mu=0.2, L=6, cooling_phase=1.0, sigma=3.5)
```

```{code-cell} ipython3
plt.plot(s.xyz[0].ravel(), s.get_density()[:, s.Nxyz[1]//2, s.Nxyz[2]//2], ':')
plt.xlabel("Lx")
plt.ylabel("n")
```

```{code-cell} ipython3
s1 = s.evolve(1000)
```

```{code-cell} ipython3
s1.t
```

```{code-cell} ipython3
plt.plot(s.xyz[0].ravel(), s.get_density()[:, s.Nxyz[1]//2, s.Nxyz[2]//2], ':')
plt.plot(s1.xyz[0].ravel(), s1.get_density()[:, s1.Nxyz[1]//2, s1.Nxyz[2]//2], alpha=0.5)
```

```{code-cell} ipython3

```
