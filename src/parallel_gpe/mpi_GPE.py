"""
01234567890123456789012345678901234567890123456789012345678901234567890123456789
"""
import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD
num_processes = comm.Get_size ()
rank = comm.Get_rank ()

class State():
    """
    Simple GPE implementation for BEC
    """ 
    hbar = 1.0
    u = 1.0
    micron = 1.0
    a_B = 5.2917721054980885238e-5 * micron #Bohr radius
    m = 87*u
    a = 100*a_B
   
    def __init__(
        self, 
        M=5, # The assigned size of the mesh 
        t=0,
        dt=0.1,
        mu=0.2,
        cooling_phase=1.0,
        g=4 * np.pi * hbar ** 2 * a / m,
        mpi=False,
        sigma=5,
        n0 = 10,
        L = 10,
    ):
        self.M = M
        # Actual number of nodes in each direction
        self.N = 2**(self.M) + 1
        # Physical size of computational box
        self.L = 2*np.pi * L * self.micron
        self.t = t
        self.dt = dt
        self.mu = mu
        self.cooling_phase = cooling_phase
        self.g = g
        self.mpi = mpi
        self.sigma = sigma
        self.n0 = n0

        self.data = np.zeros(np.asarray((self.N,)*3), dtype=complex)
        
        self.init()
        # The mesh
     
    def init(self):
        
        self.Nx, self.Ny, self.Nz = (self.N,)*3
        self.Lx, self.Ly, self.Lz = (self.L,)*3
        self.Lxyz = np.asarray((self.L,)*3)
        self.Nxyz = np.asarray((self.N,)*3)
        self.x0, self.y0, self.z0 = (self.L // 2,)*3
        self.x = np.arange(self.Nx)*self.Lx/self.Nx - self.x0
        self.y = np.arange(self.Ny)*self.Ly/self.Ny - self.y0
        self.z = np.arange(self.Nz)*self.Lz/self.Nz - self.z0

        self.xyz = np.meshgrid(*[self.x, self.y, self.z], sparse=True, 
				indexing='ij')

        self.kx = 2*np.pi * np.fft.fftshift(np.fft.fftfreq(self.Nx, 
				self.Lx/self.Nx))
        self.ky = 2*np.pi * np.fft.fftshift(np.fft.fftfreq(self.Ny, 
				self.Ly/self.Ny))
        self.kz = 2*np.pi * np.fft.fftshift(np.fft.fftfreq(self.Nz, 
				self.Lz/self.Nz))

        self.kxyz = np.meshgrid(*[self.kx, self.ky, self.kz], 
			sparse=True, indexing='ij')

        # Here we compute the kinetic energy matrix for use with the FFT.
        self.K = sum((self.hbar * _k) ** 2 / 2.0 / self.m for _k in self.kxyz)
        self._phase = 1.0 / 1j / self.hbar / self.cooling_phase

        self.metric = np.prod(np.divide(self.Lxyz, self.Nxyz))
 
        self.data[...] = np.sqrt(self.n0)*np.exp(sum((-_x**2/2/self.sigma**2)
                        for _x in self.xyz)) + self.n0
    def get_density(self):
        """Return the density of the state."""
        return abs(self.data[...]) ** 2
    def fft(self, y):
        """Return the FFT of y over the spatial indices."""
        return self._fftn(y)

    def ifft(self, y):
        """Return the IFFT of y over the spatial indices."""
        yt = self._ifftn(y)
        return yt

    def _fftn(self, y):
        """Return the FFT of y over the spatial indices.  These versions with
        an underscore may be overloaded for performance."""
        return np.fft.fftn(y)

    def _ifftn(self, y):
        """Return the IFFT of y over the spatial indices.  These versions with
        an underscore may be overloaded for performance."""
        return np.fft.ifftn(y)

    def apply_exp_K(self, dt):
        r"""Apply $e^{-i K dt}$ in place."""
        y = self.data[...]
        self.data[...] = self.ifft(np.exp(self.K * self._phase * dt) 
                            * self.fft(y))

    def apply_exp_V(self, dt, state):
        r"""Apply $e^{-i V dt}$ in place using `state` for any
        nonlinear dependence in V. (Linear problems should ignore
        `state`.)"""
        self.data[...] *= np.exp(self.g*abs(self.data[...])**2 * self._phase * dt)

    def evolve(self, steps=None):
        r"""Evolve the system by `steps`."""
        t0 = float(self.t)
        print(t0)
        assert steps > 1

        for kt in range(steps):
            dt = self.dt
            y = self
            y.apply_exp_K(dt=dt/2)
            y.t += 0.5*dt
            y.apply_exp_V(dt=dt, state=self)
            y.t += dt
            y.apply_exp_K(dt=dt/2)
            y.t += 0.5*dt 
        assert np.allclose(0.5*self.t, (t0 + steps * self.dt))
        return self

    def t(self):
        return self.t




    
        
    


